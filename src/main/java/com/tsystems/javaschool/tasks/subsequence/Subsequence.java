package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Iterator;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if ((x == null) || (y == null)) throw new java.lang.IllegalArgumentException();
        if (x.size() > y.size())  return false;
        int overlap_count = 0;
        Iterator itX = x.iterator();
        Iterator itY = y.iterator();
        Object x_element, y_element;
        while(itY.hasNext() && itX.hasNext()){
            x_element = itX.next();
       
            while (itY.hasNext()){
                y_element = itY.next();
                if (y_element == null && x_element == null) {overlap_count++; break;}
                else if(x_element != null && x_element.equals(y_element)){
                    overlap_count++;
                    break;
                }
            }
        }
       
        return overlap_count == x.size();
        }
}
