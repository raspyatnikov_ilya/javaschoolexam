package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class DuplicateFinder {
    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
       if((sourceFile == null)||(targetFile == null)) throw new IllegalArgumentException();
        Map<String, Integer> lines = new TreeMap<>();
        try(BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(targetFile, true)))
        {
            String line;
            while((line=reader.readLine())!=null){

                if (lines.containsKey(line)) lines.put(line, lines.get(line) + 1);
                else lines.put(line, 1);
            }

            for (Map.Entry<String, Integer> entry : lines.entrySet()) {
                writer.write(String.format("%s [%d]", entry.getKey(), entry.getValue()) + System.lineSeparator());
                writer.flush();
            }

        }
        catch(Exception e){
            return false;
        }

        return true;

    }
}
