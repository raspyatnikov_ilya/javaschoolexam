package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.text.DecimalFormat;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
    if (statement == null) return null;
        String statementWithoutSpaces = statement.replaceAll(" ", "");
        if (!isValid(statementWithoutSpaces))
            return null;
        try{
        return parseRPN(convertToRPN(statement));}
        catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }
    }
    /**
     *
     * @param s String statement is validated
     * @return true if input String is valid, false otherwise
     */
    private boolean isValid(String s){
        if(!s.matches("^([\\-\\+\\*/\\)\\(\\d]|(\\d+\\.?\\d+))+$") ||
                s.matches("(^[-\\+\\)\\*/])+.*") ||
                s.matches(".*([\\*/\\(\\+\\-]$)+") ||
                s.matches(".*([\\+\\-\\*/][\\+\\-\\*/\\)$])+.*") ||
                s.matches(".*((\\d|(\\d+\\.?\\d+))[\\(])+.*") ||
                s.matches(".*(\\([-\\+\\*/\\)$])+.*") ||
                s.matches(".*(\\)([\\d\\(]|(\\d+\\.?\\d+)))+.*"))
            return false;

        int k = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '('){
                k++;
            } else if(s.charAt(i) == ')') k--;
            if(k < 0) return false;
            if(i == s.length()-1 && k > 0) return false;
        }
        return true;
    }

    /**
     * Convert statement from infix notation to Reverse Polish notation
     * @param input validated input statement
     * @return String value represetning input statement in RPN
     */
    public String convertToRPN(String input){
        Map<String, Integer> mathOperators = new HashMap<>();
            mathOperators.put("*", 1);
            mathOperators.put("/", 1);
            mathOperators.put("+", 2);
            mathOperators.put("-", 2);

       Set<String> allOperators = new HashSet<>(mathOperators.keySet());

            allOperators.add("(");
            allOperators.add(")");
        Stack<String> stack = new Stack<>();
        ArrayList<String> output = new ArrayList<>();

        int index = 0;
        boolean hasNext = true;

        while (hasNext){
            int nextOperatorIndex = input.length();
            String nextOperator = "";
            for (String operator: allOperators){
                int i = input.indexOf(operator, index);
                if (i >= 0 && i < nextOperatorIndex){
                    nextOperator = operator;
                    nextOperatorIndex = i;
                }
            }

            if (nextOperatorIndex == input.length()){
                hasNext = false;
            }
            else {
                if (index != nextOperatorIndex){
                    output.add(input.substring(index, nextOperatorIndex));
                }
                switch (nextOperator) {
                    case "(":
                        stack.push(nextOperator);
                        break;
                    case ")":
                        while (!stack.peek().equals("(")) {
                            output.add(stack.pop());
                        }
                        stack.pop();
                        break;
                    default:
                        while (!stack.isEmpty() && !stack.peek().equals("(")
                                && (mathOperators.get(nextOperator) >= mathOperators.get(stack.peek()))) {
                            output.add(stack.pop());
                        }
                        stack.push(nextOperator);
                        break;
                }
                index = nextOperatorIndex + nextOperator.length();
            }
        }
        if (index != input.length()){
            output.add(input.substring(index));
        }
        while (!stack.isEmpty()){
            output.add(stack.pop());
        }

        StringBuilder result = new StringBuilder();
        if (!output.isEmpty()){
            result.append(output.remove(0));
        }
        while (!output.isEmpty()){
            result.append(" ").append(output.remove(0));
        }
        return result.toString();
    }

    /**
     *Parsing and computing result of input statement
     * @param input String in RPN
     * @return String value of computed input statement
     */
    public String parseRPN(String input){
        Stack<Double> stack = new Stack<>();
        StringTokenizer tokenizer = new StringTokenizer(input, " ");
        double num1, num2, interAns;

        while (tokenizer.hasMoreTokens()){
            String token = tokenizer.nextToken();
            switch(token){
                case "+":
                case "-":
                case "/":
                case "*":
                    num2 = stack.pop();
                    num1 = stack.pop();
                    switch (token){
                        case "+":
                            interAns = num1 + num2;
                            break;
                        case "-":
                            interAns = num1 - num2;
                            break;
                        case "*":
                            interAns = num1 * num2;
                            break;
                        case "/":
                            interAns = num1 / num2;
                            break;
                        default:
                            interAns = 0;
                    }
                    stack.push(interAns);
                    break;
                default: stack.push(new Double(token)); break;

            }
        }
        interAns = stack.pop();
        if (interAns == Double.POSITIVE_INFINITY || interAns == Double.NEGATIVE_INFINITY ||interAns == Double.NaN) return null;
        DecimalFormat format = new DecimalFormat("#.####");

        return format.format(interAns);
    }
}